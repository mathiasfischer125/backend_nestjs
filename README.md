# Wydin backend

## Installation

```bash
$ yarn add
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Database Migration

```bash
# create a init migration  if you made any change for database you should need to run given below 2 commands before yarn run start
$ yarn run typeorm:migration:generate -- my_init

# Run a migration
yarn run typeorm:migration:run

# add seed data
$ yarn run seed
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```
