import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as XLSX from 'xlsx';
import { WorkSheet } from 'xlsx';
import { Activity } from '../model/activity/activity.entity';
import { Division } from '../model/division/division.entity';
import { Class } from '../model/class/class.entity';

type SeedItem = {
  Level: number;
  Code: string;
  Activité: string;
  Division: string;
  Classe: string;
};

@Injectable()
export class SeedService {
  constructor(
    @InjectRepository(Activity)
    private activityRepo: Repository<Activity>,
    @InjectRepository(Division)
    private divisionRepo: Repository<Division>,
    @InjectRepository(Class)
    private classRepo: Repository<Class>,
  ) {}

  async run() {
    const xlsx = this.readingXlsx();
    const jsonData: SeedItem[] = XLSX.utils.sheet_to_json(xlsx);

    let activityInstance: Activity = {} as Activity;
    let divisionInstance: Division = {} as Division;

    for (const data of jsonData) {
      switch (data.Level) {
        case 1: {
          const activity = await this.activityRepo.create({
            text: data.Activité,
          });
          await activity.save();
          activityInstance = activity;
          break;
        }

        case 2: {
          const division = await this.divisionRepo.create({
            text: data.Division,
            activity: activityInstance,
          });
          await division.save();
          divisionInstance = division;
          break;
        }

        case 3: {
          const classe = await this.classRepo.create({
            text: data.Classe,
            division: divisionInstance,
          });
          await classe.save();
          break;
        }

        default:
          break;
      }
    }
  }

  private readingXlsx(): WorkSheet {
    const xlsxData = XLSX.readFile('src/data/Activities.xlsx');
    return xlsxData.Sheets['NAF_N'];
  }
}
