import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Activity } from '../../model/activity/activity.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ActivityService {
  constructor(
    @InjectRepository(Activity)
    private readonly repository: Repository<Activity>,
  ) {}

  public async getAll() {
    try {
      return await this.repository.find({
        relations: ['divisions', 'divisions.classes'],
      });
    } catch (error) {
      throw new HttpException(
        'Fetching activities is failed',
        HttpStatus.BAD_REQUEST,
      );
    }
  }
}
