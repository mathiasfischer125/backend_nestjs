import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  BaseEntity,
  OneToMany,
  Index,
} from 'typeorm';
import { Activity } from '../activity/activity.entity';
import { Class } from '../class/class.entity';

@Entity()
export class Division extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: number;

  @ManyToOne(() => Activity, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'activity_id', referencedColumnName: 'id' })
  @Index()
  activity: Activity;

  @OneToMany(() => Class, (classes) => classes.division)
  classes: Class[];

  @Column({ nullable: false })
  text: string;

  @CreateDateColumn({ type: 'timestamp' })
  expiredAt: Date;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;
}
