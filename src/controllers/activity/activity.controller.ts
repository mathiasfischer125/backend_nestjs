import { Controller, Get, HttpCode, Req, Res } from '@nestjs/common';
import { ActivityService } from '../../services/activity/activity.service';

import { ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('activities')
@Controller('activities')
export class ActivityController {
  constructor(private service: ActivityService) {}

  @Get('')
  @HttpCode(200)
  @ApiOperation({ summary: 'fetching all apis' })
  public async getActivities(@Req() req, @Res() res) {
    try {
      const response = await this.service.getAll();

      res.status(200).json({
        status: 'Success',
        data: response,
      });
    } catch (error) {
      res.status(400).json({
        status: 'Error',
        message: error.message,
      });
    }
  }
}
