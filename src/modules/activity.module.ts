import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ActivityService } from '../services/activity/activity.service';
import { ActivityController } from '../controllers/activity/activity.controller';
import { SeedService } from '../services/seed.service';
import { Activity } from '../model/activity/activity.entity';
import { Division } from '../model/division/division.entity';
import { Class } from '../model/class/class.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Activity]),
    TypeOrmModule.forFeature([Division]),
    TypeOrmModule.forFeature([Class]),
  ],
  providers: [ActivityService, SeedService],
  controllers: [ActivityController],
  exports: [],
})
export class ActivityModule {}
